package com.example.checker;

public enum RuleResult {
    Ok, Error, Exception
}