package com.example.checker.rules;

import com.example.checker.ICanCheckRule;
import com.example.domain.User;
import com.example.checker.CheckResult;
import com.example.checker.RuleResult;

public class LoginRule implements ICanCheckRule<User> {
    public CheckResult checkRule(User entity) {

        if (entity.getLogin() == null)
            return new CheckResult("", RuleResult.Error);
        if (entity.getLogin().equals(""))
            return new CheckResult("", RuleResult.Error);
        return new CheckResult("", RuleResult.Ok);
    }
}
