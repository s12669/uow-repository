package com.example.checker.rules;

import com.example.checker.CheckResult;
import com.example.checker.ICanCheckRule;
import com.example.checker.RuleResult;
import com.example.domain.Person;

public class NameRule implements ICanCheckRule<Person> {

    public CheckResult checkRule(Person entity) {

        if (entity.getFirstName() == null)
            return new CheckResult("", RuleResult.Error);
        if (entity.getFirstName().equals(""))
            return new CheckResult("", RuleResult.Error);
        return new CheckResult("", RuleResult.Ok);
    }
}
