package com.example.checker.rules;

import com.example.checker.ICanCheckRule;
import com.example.domain.User;
import com.example.checker.CheckResult;
import com.example.checker.RuleResult;

public class PasswordRule implements ICanCheckRule<User>{
    public CheckResult checkRule(User entity) {

        if (entity.getPassword() == null)
            return new CheckResult("", RuleResult.Error);
        if (entity.getPassword().equals(""))
            return new CheckResult("", RuleResult.Error);
        if(entity.getPassword().length() < 8)
            return new CheckResult("",RuleResult.Error);
        return new CheckResult("", RuleResult.Ok);
    }
}
