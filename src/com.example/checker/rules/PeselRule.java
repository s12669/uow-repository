package com.example.checker.rules;

import com.example.checker.CheckResult;
import com.example.checker.ICanCheckRule;
import com.example.checker.RuleResult;
import com.example.domain.Person;

public class PeselRule implements ICanCheckRule<Person> {

    public CheckResult checkRule(Person entity) {

        int[] weights = {1, 3, 7, 9, 1, 3, 7, 9, 1, 3};
        int sum = 0;
        int mod;
        int ctrl = 0;
        if (entity.getPesel() != null) {
            if (!entity.getPesel().equals("")) {
                for (int i = 0; i < weights.length; i++) {
                    sum += Integer.parseInt(entity.getPesel().substring(i, i + 1)) * weights[i];
                }
                mod = sum % 10;
                if (mod == 0) ctrl = 0;
                else ctrl = 10 - mod;
            }
        }

        if (entity.getPesel() == null)
            return new CheckResult("", RuleResult.Error);
        if (entity.getPesel().equals(""))
            return new CheckResult("", RuleResult.Error);
        if (entity.getPesel().length() != 11)
            return new CheckResult("", RuleResult.Error);
        if (Integer.parseInt(entity.getPesel().substring(10)) != ctrl)
            return new CheckResult("", RuleResult.Error);
        return new CheckResult("", RuleResult.Ok);
    }
}