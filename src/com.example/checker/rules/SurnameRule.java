package com.example.checker.rules;

import com.example.checker.CheckResult;
import com.example.checker.ICanCheckRule;
import com.example.checker.RuleResult;
import com.example.domain.Person;

public class SurnameRule implements ICanCheckRule<Person> {

    public CheckResult checkRule(Person entity) {

        if (entity.getSurname() == null)
            return new CheckResult("", RuleResult.Error);
        if (entity.getSurname().equals(""))
            return new CheckResult("", RuleResult.Error);
        return new CheckResult("", RuleResult.Ok);
    }
}
