package com.example.domain;

public enum EntityState {
    New, Modified, UnChanged, Deleted, Unknown
}
