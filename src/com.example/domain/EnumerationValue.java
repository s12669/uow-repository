package com.example.domain;

public class EnumerationValue extends Entity {
    private String intKey;
    private String stringKey;
    private String value;
    private String enumerationName;

    public String getIntKey() {
        return intKey;
    }

    public void setIntKey(String intKey) {
        this.intKey = intKey;
    }

    public String getEnumerationName() {
        return enumerationName;
    }

    public void setEnumerationName(String enumerationName) {
        this.enumerationName = enumerationName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getStringKey() {
        return stringKey;
    }

    public void setStringKey(String stringKey) {
        this.stringKey = stringKey;
    }
}
