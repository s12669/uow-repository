package com.example.domain;

public class PhoneNumber extends Entity{
    private int countryPrefix;
    private int cityPrefix;
    private long number;
    private int typeId;

    public int getCountryPrefix() {
        return countryPrefix;
    }

    public void setCountryPrefix(int countryPrefix) {
        this.countryPrefix = countryPrefix;
    }

    public int getCityPrefix() {
        return cityPrefix;
    }

    public void setCityPrefix(int cityPrefix) {
        this.cityPrefix = cityPrefix;
    }

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }
}
