package com.example.repository;

import com.example.domain.Person;

public interface IPersonRepository extends IRepository<Person>{
    Person getByFirstName(String firstName);

    Person getByLastName(String lastName);
}
