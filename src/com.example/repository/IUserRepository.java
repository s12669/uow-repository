package com.example.repository;

import com.example.domain.User;

public interface IUserRepository extends IRepository<User> {
    User withLogin(String login);
    User withLoginAndPassword(String login, String password);
    User setupPermissions(User user);
    User setupRoles(User user);
}
