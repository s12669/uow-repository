package com.example.repository.implementation;

import com.example.builder.implementation.EnumerationValueBuilder;
import com.example.builder.implementation.PersonBuilderI;
import com.example.builder.implementation.UserBuilderI;
import com.example.repository.IEnumerationValueRepository;
import com.example.repository.IPersonRepository;
import com.example.repository.IRepositoryCatalog;
import com.example.repository.IUserRepository;
import com.example.uowrepository.implementation.UnitOfWork;

import java.sql.Connection;

public class RepositoryCatalog implements IRepositoryCatalog {

    private Connection connection;
    private UnitOfWork unitOfWork;

    public RepositoryCatalog(Connection connection, UnitOfWork unitOfWork) {
        this.connection = connection;
        this.unitOfWork = unitOfWork;
    }

    @Override
    public IEnumerationValueRepository enumeration() {
        return new EnumerationValueRepository(this.connection, new EnumerationValueBuilder(), this.unitOfWork);
    }

    @Override
    public IUserRepository users() {
        return new UserRepository(this.connection, new UserBuilderI(), this.unitOfWork);
    }

    @Override
    public IPersonRepository persons() {
        return new PersonRepository(this.connection, new PersonBuilderI(), this.unitOfWork);
    }
}
