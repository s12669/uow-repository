import com.example.domain.User;
import com.example.checker.CheckResult;
import com.example.checker.RuleResult;
import com.example.checker.rules.LoginRule;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class LoginRuleTest {
    LoginRule rule = new LoginRule();

    @Test
    public void checker_should_check_if_the_user_login_is_not_null() {
        User u = new User();
        CheckResult result = rule.checkRule(u);
        assertTrue(result.getResult().equals(RuleResult.Error));

    }

    @Test
    public void checker_should_check_if_the_user_login_is_not_empty() {
        User u = new User();
        u.setLogin("");
        CheckResult result = rule.checkRule(u);
        assertTrue(result.getResult().equals(RuleResult.Error));

    }

    @Test
    public void checker_should_return_OK_if_the_login_is_not_null() {
        User u = new User();
        u.setLogin("janek123");
        CheckResult result = rule.checkRule(u);
        assertTrue(result.getResult().equals(RuleResult.Ok));

    }
}
