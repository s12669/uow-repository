import com.example.domain.Person;
import com.example.checker.CheckResult;
import com.example.checker.RuleResult;
import com.example.checker.rules.NameRule;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class NameRuleTest {

    NameRule rule = new NameRule();

    @Test
    public void checker_should_check_if_the_person_name_is_not_null() {
        Person p = new Person();
        CheckResult result = rule.checkRule(p);
        assertTrue(result.getResult().equals(RuleResult.Error));

    }

    @Test
    public void checker_should_check_if_the_person_name_is_not_empty() {
        Person p = new Person();
        p.setFirstName("");
        CheckResult result = rule.checkRule(p);
        assertTrue(result.getResult().equals(RuleResult.Error));

    }

    @Test
    public void checker_should_return_OK_if_the_name_is_not_null() {
        Person p = new Person();
        p.setFirstName("Jan");
        CheckResult result = rule.checkRule(p);
        assertTrue(result.getResult().equals(RuleResult.Ok));

    }
}